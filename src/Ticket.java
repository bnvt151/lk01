public class Ticket {
    // Tulis kode disini
    // atribut tiket penumpang kereta
    private String nama;
    private String asal;
    private String tujuan;


    //konstruktor data tiket penumpang kereta Komuter
    public Ticket(String nama){
        this.nama = nama;
    }

    // Konstruktor data tiket penumpang KAJJ
    public Ticket(String nama, String asal, String tujuan){
        this.nama = nama;
        this.asal = asal;
        this.tujuan = tujuan;
    }

    // method untuk mengakses namapenumpang kedua kereta
    public String getNama(){
        return nama;
    }
    
    // method untuk mengakses nama penumpang KAJJ
    public String getAsal(){
        return asal;
    }
    // method untuk mengakses nama penumpang KAJJ
    public String getTujuan(){
        return tujuan;
    }
}